<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<html>
<head><title>Title</title></head>
<body>

<div class="col-4 mx-auto mt-lg-3 border border-info p-3 rounded">
	<form action="/login" method="post">
		Login : <input type="login" class="form-control" name="login_parameter">
		Password : <input type="password" class="form-control" name="password_parameter"><br>
		<input type="submit" class="btn btn-primary" value="Sign In">
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>
</div>

</body>
</html>