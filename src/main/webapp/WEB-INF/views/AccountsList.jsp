<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Askar
  Date: 15.12.2018
  Time: 18:27
  To change this template use File | Settings | File Templates.
--%>
<link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>Title</title>
</head>
<body>

<div class="col-10 mx-auto mt-lg-3 border border-info p-3 rounded">
	<table class="table table-bordered table-sm">
		<tr class="bg-primary">
			<th>Last name</th>
			<th>First Name</th>
			<th>IIN</th>
			<th>Balance</th>
			<th></th>
			<th></th>
		</tr>
		<c:forEach items="${accountsList}" var="account">
			<tr>
				<td>${account.last_name}</td>
				<td>${account.first_name}</td>
				<td>${account.iin_number}</td>
				<td>${account.amount} ${account.currency_id.short_name}</td>
				<td>
					<form action="seeAccountHistory" method="get">
						<input type="hidden" name="id" value="${account.id}"/>
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
						<button type="submit" class="btn btn-primary btn-sm">History</button>
					</form>
				</td>
				<td>
					<form action="userOperation" method="post">
						<select class="form-control form-control-sm" name="operation">
							<c:forEach items="${operationsList}" var="op">
								<option value="${op.id}">${op.name}</option>
							</c:forEach>
						</select>
						<input type="hidden" name="id" value="${account.id}"/>
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
						<button type="submit" class="btn btn-primary btn-sm">Operations</button>
					</form>
				</td>
			</tr>
		</c:forEach>
	</table>
	<form action="managerPage" method="get">
		<button type="submit" class="btn btn-primary">Go back</button>
	</form>
</div>

</body>
</html>
