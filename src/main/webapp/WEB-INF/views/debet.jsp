<%--
  Created by IntelliJ IDEA.
  User: Askar
  Date: 16.12.2018
  Time: 15:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<html>
<head>
	<title>Title</title>
</head>
<body>

<div class="col-4 mx-auto mt-lg-3 border border-info p-3 rounded">
	<form action="debetOperation" method="post">
		Balance: ${account.amount} ${account.currency_id.short_name}<br>
		Value: <input type="text" name="value" class="form-control"><br>
		<input type="hidden" name="id" value="${account.id}"/>
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<button type="submit" class="btn btn-primary">Submit</button>
	</form>
	<form action="accountsListPage" method="get">
		<button type="submit" class="btn btn-primary">Go back</button>
	</form>
</div>


</body>
</html>
