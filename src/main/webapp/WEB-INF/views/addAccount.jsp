<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Askar
  Date: 08.12.2018
  Time: 22:57
  To change this template use File | Settings | File Templates.
--%>
<link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>Title</title>
</head>
<body>

<div class="col-4 mx-auto mt-lg-3 border border-info p-3 rounded">
	<form action="addAccount" method="post">
		First name : <input type="text" name="fname" class="form-control">
		Last name : <input type="text" name="lname" class="form-control">
		Birth Date : <input type="text" name="bdate" class="form-control">
		IIN : <input type="text" name="iin" class="form-control">
		Currency :
		<select class="form-control" name="currency">
			<c:forEach items="${currencies}" var="cur">
				<option value="${cur.id}">${cur.short_name}</option>
			</c:forEach>
		</select>
		Installment : <input type="text" name="amount" class="form-control"><br>
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<button type="submit" class="btn btn-primary">Create account</button>
	</form>

	<form action="managerPage" method="get">
		<button type="submit" class="btn btn-primary">Go back</button>
	</form>
</div>

</body>
</html>
