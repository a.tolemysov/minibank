<%@ page import="com.myspring.entities.Users" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head><title>Title</title></head>
<body>
<%
	Users user = (Users) request.getAttribute("user");
%>

<h1>PROFILE PAGE OF <%=user.getFull_name() %></h1><br>
<a href="/admin">ADMIN PAGE</a>
<a href="/managerPage">MANAGER PAGE</a>

<form action="/logout" method="post">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	<input type="submit" value="LOGOUT">
</form>

</body>
</html>