<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Askar
  Date: 16.12.2018
  Time: 20:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<html>
<head>
	<title>Title</title>
	<script>
        function validateForm() {
            var x = document.forms["myForm"]["value"].value;
            if (x > ${account.amount}) {
                alert("Value exceed account's balance");
                return false;
            } else if (x >= 1 && x <= ${account.amount}){
                return true;
            } else {
                alert("Incorrect input");
                return false;
            }
        }
	</script>
</head>
<body>

<div class="col-4 mx-auto mt-lg-3 border border-info p-3 rounded">
	<form name="myForm" action="transferOperation" onsubmit="return validateForm()" method="post">
		Balance: ${account.amount} ${account.currency_id.short_name}<br>
		Insert user's IIN: <input type="text" name="targetIin" class="form-control" list="accounts"><p style="color: red">${error}</p>
		<datalist id="accounts">
			<c:forEach items="${accountsList}" var="account">
				<option value="${account.iin_number}">${account.last_name} ${account.first_name}</option>
			</c:forEach>
		</datalist>
		Value: <input type="text" name="value" class="form-control"><br>
		<input type="hidden" name="id" value="${account.id}"/>
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<button type="submit" class="btn btn-primary">Submit</button>
	</form>
    <form action="accountsListPage" method="get">
        <button type="submit" class="btn btn-primary">Go back</button>
    </form>
</div>

</body>
</html>
