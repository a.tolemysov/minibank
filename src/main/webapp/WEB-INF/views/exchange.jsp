<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Askar
  Date: 18.12.2018
  Time: 11:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<html>
<head>
    <title>Title</title>
</head>
<body>

<div class="col-4 mx-auto mt-lg-3 border border-info p-3 rounded">
    <form action="exchangeOperation" method="post">
        <c:forEach items="${currencies}" var="cur">
            <c:if test="${cur.id == account.currency_id.id}">
                <input name="newCurrencyId" type="radio" value="${cur.id}" checked>${cur.short_name}<br>
                <input type="hidden" name="currentCurrencyId" value="${cur.id}"/>
            </c:if>
            <c:if test="${cur.id != account.currency_id.id}">
                <input name="newCurrencyId" type="radio" value="${cur.id}">${cur.short_name}<br>
            </c:if>
        </c:forEach>
        <input type="hidden" name="id" value="${account.id}"/>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/><br>
        <button type="submit" class="btn btn-primary">Confirm</button>
    </form>
    <form action="accountsListPage" method="get">
        <button type="submit" class="btn btn-primary">Go back</button>
    </form>
</div>

</body>
</html>
