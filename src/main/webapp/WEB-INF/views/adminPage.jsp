<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Askar
  Date: 02.12.2018
  Time: 16:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<html>
<head>
	<title>ADMIN</title>
</head>
<body>

<div class="col-4 mx-auto mt-lg-3 border border-info p-3 rounded">
	<div style="float: right">
		<table class="table table-info p-1">
		<c:forEach items="${balance}" var="balance">
			<tr>
				<td>${balance.value}</td>
				<td>${balance.currency.short_name}</td>
			</tr>
		</c:forEach>
		</table>
	</div>

	<a class="btn btn-primary" href="addManagerPage">Add new manager</a><br><br>
	<a class="btn btn-primary" href="transactionListPage">List of transactions</a><br><br>
	<a class="btn btn-primary" href="currencyPage">Change currency</a><br><br>

	<form action="/logout" method="post">
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<input type="submit" class="btn btn-secondary btn-block" value="Logout">
	</form>
</div>
</body>
</html>
