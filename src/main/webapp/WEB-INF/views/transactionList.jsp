<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Askar
  Date: 08.12.2018
  Time: 20:31
  To change this template use File | Settings | File Templates.
--%>
<link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<div class="col-10 mx-auto mt-lg-3 border border-info p-3 rounded">
    <table class="table table-bordered table-sm">
        <tr class="bg-primary">
            <th>Manager</th>
            <th>From</th>
            <th>To</th>
            <th>Operation</th>
            <th>Value</th>
            <th>Date</th>
        </tr>
        <c:forEach items="${transactionList}" var="list">
            <tr>
                <td>${list.manager_id.full_name}</td>
                <td>${list.sender_account_id.last_name} ${list.sender_account_id.first_name}</td>
                <td>${list.receiver_account_id.last_name} ${list.receiver_account_id.first_name}</td>
                <td>${list.operation_id.name}</td>
                <td>${list.amount}</td>
                <td>${list.operation_time}</td>
            </tr>
        </c:forEach>
    </table>

    <%
        int role = (int) request.getAttribute("role");
        if (role == 1) {
    %>
    <form action="admin" method="get">
        <button type="submit" class="btn btn-primary">Go back</button>
    </form>
    <%
    } else {
        if (request.getAttribute("accountId") != null) {
    %>

    <form action="generateFile" method="post">
        <input type="hidden" name="id" value="${accountId}"/>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <button type="submit" class="btn btn-primary">Print</button>
    </form>
    <form action="accountsListPage" method="get">
        <button type="submit" class="btn btn-primary">Go back</button>
    </form>
    <%
        } else {
    %>
    <form action="managerPage" method="get">
        <button type="submit" class="btn btn-primary">Go back</button>
    </form>
    <%
            }
        }
    %>

</div>
</body>
</html>
