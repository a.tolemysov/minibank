<%--
  Created by IntelliJ IDEA.
  User: Askar
  Date: 08.12.2018
  Time: 20:03
  To change this template use File | Settings | File Templates.
--%>
<link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>Title</title>
</head>
<body>

<div class="col-4 mx-auto mt-lg-3 border border-info p-3 rounded">
	<form action="addManager" method="post">
		Login : <input type="text" name="login" class="form-control">
		Password : <input type="text" name="password" class="form-control">
		Full name : <input type="text" name="fullName" class="form-control"><br>
		<button type="submit" class="btn btn-primary">Create manager</button>
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>

	<form action="admin" method="get">
		<button type="submit" class="btn btn-primary">Go back</button>
	</form>
</div>

</body>
</html>
