<%--
  Created by IntelliJ IDEA.
  User: Askar
  Date: 08.12.2018
  Time: 22:55
  To change this template use File | Settings | File Templates.
--%>
<link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>Title</title>
</head>
<body>

<div class="col-4 mx-auto mt-lg-3 border border-info p-3 rounded">
	<a class="btn btn-primary" href="addAccountPage">Add account</a><br><br>
	<a class="btn btn-primary" href="accountsListPage">Accounts</a><br><br>
	<a class="btn btn-primary" href="seeManagerHistory">Transaction history</a><br><br>

	<form action="/logout" method="post">
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<input type="submit" class="btn btn-secondary btn-block" value="Logout">
	</form>
</div>

</body>
</html>
