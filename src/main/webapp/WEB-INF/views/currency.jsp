<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Askar
  Date: 08.12.2018
  Time: 22:22
  To change this template use File | Settings | File Templates.
--%>
<link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>Title</title>
</head>
<body>

<div class="col-4 mx-auto mt-lg-3 border border-info p-3 rounded">
	<c:forEach items="${curList}" var="list">

		<form action="editCurrencies" method="post">
			<table class="table table-bordered table-sm">
				<tr class="bg-primary">
					<th>${list.currency_id.short_name}</th>
					<th></th>
				</tr>
				<tr>
					<td>Purchase<input type="text" name="buy" class="form-control form-control-sm" value="${list.purchase_value}"></td>
					<td>Sale<input type="text" name="sale" class="form-control form-control-sm" value="${list.sale_value}"></td>
				</tr>
				<tr>
					<input type="hidden" name="id" value="${list.id}"/>
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					<td><button type="submit" class="btn btn-primary">Confirm</button></td>
				</tr>
			</table>
		</form>
	</c:forEach>

	<form action="admin" method="get">
		<button type="submit" class="btn btn-primary">Go back</button>
	</form>
</div>

</body>
</html>
